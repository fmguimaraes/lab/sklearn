#https://medium.com/value-stream-design/online-machine-learning-515556ff72c5
import pandas as pd
import numpy as np
from collections import Mapping, Sequence 
from sklearn import linear_model
from sklearn.externals import joblib

regression_model = joblib.load('trained_partial_model.pkl') 

n_samples, n_features = 1, 500
y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)
regression_model.partial_fit(X, y)

joblib.dump(regression_model, 'trained_partial_model.pkl') 