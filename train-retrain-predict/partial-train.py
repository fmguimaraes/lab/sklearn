#https://medium.com/value-stream-design/online-machine-learning-515556ff72c5
import pandas as pd
import numpy as np
from collections import Mapping, Sequence 
from sklearn import linear_model
from sklearn.externals import joblib
import time

n_samples, n_features = 50000, 500
y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)
clf = linear_model.SGDRegressor()

start_time = time.time()
clf.partial_fit(X, y)
elapsed_time = time.time() - start_time

print('time elapsed for training: {}'.format(elapsed_time))

joblib.dump(clf, 'trained_partial_model.pkl') 