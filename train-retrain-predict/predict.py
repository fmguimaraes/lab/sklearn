#http://benalexkeen.com/linear-regression-in-python-using-scikit-learn/
#http://scikit-learn.org/stable/modules/model_persistence.html
import pandas as pd
from collections import Mapping, Sequence 
from sklearn.externals import joblib
regression_model = joblib.load('trained_model.pkl') 


first = regression_model.predict([[4, 121, 110, 2800, 15.4, 81, 0]])
print('prediction from trained model: {}'.format(first))