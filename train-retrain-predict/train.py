#http://benalexkeen.com/linear-regression-in-python-using-scikit-learn/
#http://scikit-learn.org/stable/modules/model_persistence.html
import pandas as pd
import numpy as np
from collections import Mapping, Sequence 
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import math
from sklearn.externals import joblib

df = pd.read_csv('auto-mpg.csv')
df = df.drop('name', axis=1)#removing car name

df = df.replace('?', np.nan)#removing missing values
df = df.dropna()

#split data
X = df.drop('mpg', axis=1)
y = df[['mpg']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1)

#train 
regression_model = SGDRegressor()
regression_model.fit(X_train, y_train)
LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)

#trained coeficients
#for idx, col_name in enumerate(X_train.columns):
#    print("The coefficient for {} is {}".format(col_name, regression_model.coef_[0][idx]))


intercept = regression_model.intercept_[0]
#print("The intercept for our model is {}".format(intercept))

#scoring
firstScoreModel = regression_model.score(X_test, y_test)
#print('first score model: {}'.format(firstScoreModel));

y_predict = regression_model.predict(X_test)
regression_model_mse = mean_squared_error(y_predict, y_test)
#print('second score model: {}'.format(regression_model_mse))

magin_error = math.sqrt(regression_model_mse)
print('margin error: {}'.format(magin_error))

df['origin'] = df['origin'].replace({1: 'america', 2: 'europe', 3: 'asia'}) # names instead of numbers
#print(df.head())

#first = regression_model.predict([[4, 121, 110, 2800, 15.4, 81, 0]])
#print('first prediction: {}'.format(first))

joblib.dump(regression_model, 'trained_model.pkl') 