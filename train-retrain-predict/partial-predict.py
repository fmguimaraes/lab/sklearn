#http://scikit-learn.org/stable/modules/model_persistence.html
#https://medium.com/value-stream-design/online-machine-learning-515556ff72c5
import pandas as pd
import numpy as np
from collections import Mapping, Sequence 
from sklearn import linear_model
from sklearn.externals import joblib

n_samples, n_features = 1, 500
regression_model = joblib.load('trained_partial_model.pkl') 
X = np.random.randn(n_samples, n_features)

prediction = regression_model.predict(X)
print('prediction from retrained model: {}'.format(prediction))